import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:plant_app_flutter/constants.dart';
import 'package:plant_app_flutter/screens/home/components/header_with_searchbox.dart';
import 'package:plant_app_flutter/screens/home/components/recommended_plants.dart';
import 'package:plant_app_flutter/screens/home/components/title_with_more_btn.dart';

import 'featured_plants.dart';

class Body extends StatelessWidget {
  const Body({super.key});

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: Column(
        children: [
          HeaderWithSearchBox(size: size),
          TitleWithMoreBtn(
            title: 'Recommended',
            onPress: () {},
          ),
          RecommendPlants(),
          TitleWithMoreBtn(title: 'Featured Plants', onPress: () {}),
          FeaturedPlants(),
          SizedBox(height: kDefaultPadding,)
        ],
      ),
    );
  }
}


